import pygame
import os
from ScreenMove import *
import PersonObject as person

_image_library = {}
clock = pygame.time.Clock()

screen = pygame.display.set_mode((WHIDTH, HEIGHT))

def get_image(path):
    global _image_library
    image = _image_library.get(path)
    if image == None:
        canonicalized_path = path.replace('/', os.sep).replace('\\', os.sep)
        image = pygame.image.load(canonicalized_path)
        _image_library[path] = image
    return image

def get_text(text, x, y):
    size = 50
    color = (200, 000, 000)
    font_type = 'ARCADEPI.TTF'
    global screen
    text = str(text)
    font = pygame.font.Font(font_type, size)
    text = font.render(text, True, color)
    screen.blit(text, (x, y))


def PrintPlayer_zone(color , line, zone, head):
    for val in zone:
        pygame.draw.rect(screen, color.zone,
                         pygame.Rect(val.data_x_min * STRIDE, val.data_y * STRIDE,
                                     (val.data_x_max - val.data_x_min + 1) * STRIDE, STRIDE))

def PrintPlayer_line_head(color , line, zone, head):
    for val in line:
        pygame.draw.rect(screen, color.line,
                pygame.Rect(val.x*STRIDE, val.y*STRIDE, STRIDE, STRIDE))

    pygame.draw.rect(screen, color.head,
                pygame.Rect(head.x * STRIDE, head.y * STRIDE, STRIDE, STRIDE))

def PrintGameOver(GameOverObject):
    get_text("Game Over", 50, 50)
    pygame.display.flip()
    clock.tick(10)

def Render(botPlayer):
    global screen
    screen.fill((255, 255, 255))
    ScreenMove(person, botPlayer)
    if person.live == False:
        PrintGameOver(person);
    elif botPlayer.live == False:
        PrintGameOver(botPlayer);
    else:
        PrintPlayer_zone(person.color, person.line, person.zone, person.head)
        PrintPlayer_zone(botPlayer.color, botPlayer.line, botPlayer.zone, botPlayer.head)
        PrintPlayer_line_head(person.color, person.line, person.zone, person.head)
        PrintPlayer_line_head(botPlayer.color, botPlayer.line, botPlayer.zone, botPlayer.head)
        get_text("Hello", 50, 50)
    pygame.display.flip()
    clock.tick(15)